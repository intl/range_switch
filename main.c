#include <iostm8s003f3.h>
#include <intrinsics.h>

#include <stdio.h>

#define TRIG_PIN 3
#define ECHO_PIN 6
#define OUT_PIN 1
#define OUT_INV_PIN 2

#define DISTANCE_ON 120

typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t;

uint8_t count = 0;
volatile uint8_t stat = 0;

#pragma vector=TIM4_OVR_UIF_vector
__interrupt __root  void TIM4_OVR(void)
{
  count++;
  if(count==255)
    {
    uint16_t data = TIM1_CNTRH;
    data <<=8;
    data |= TIM1_CNTRL;
    data /= 58;
    if(data < DISTANCE_ON)
      {
      PD_ODR |= (1<<OUT_PIN)|(1<<OUT_INV_PIN);
      }
    else
      {
      PD_ODR &= ~((1<<OUT_PIN)|(1<<OUT_INV_PIN));
      }
    stat = 1;
    TIM4_CR1 &= ~MASK_TIM4_CR1_CEN;
    }
  TIM4_SR_bit.UIF = 0;
}


static void sys_cfg(void)
{
  CLK_PCKENR1 = 0;
  CLK_PCKENR2 = 0;
  CLK_CKDIVR = 0;
  CLK_ICKR_bit.HSIEN = 1;
  CLK_ECKR_bit.HSEEN = 0;
  while(!CLK_ICKR_bit.HSIRDY);
  CLK_SWR = 0xe1;
  
}

void timer1_init(void)
{
   CLK_PCKENR1 |= 0x80;
   TIM1_PSCRH = 0;
   TIM1_PSCRL = 0x0f;
   TIM1_ARRH = 0xff;       
   TIM1_ARRL = 0xff;       
   TIM1_CCR1H = 0x0;      
   TIM1_CCR1L = 0x0;
   TIM1_CCER1 &= ~MASK_TIM1_CCER1_CC1P;
   TIM1_CCMR1 |= (1<<0);   
   TIM1_SMCR = (1<<4)|(1<<6)|(MASK_TIM1_SMCR_SMS&5);
   TIM1_CR1  = MASK_TIM1_CR1_OPM;
}


void port_init(void)
{
  PC_CR1 |= (1<<TRIG_PIN); 
  PC_DDR |= (1<<TRIG_PIN);
  PD_CR1 |= (1<<OUT_PIN)|(1<<OUT_INV_PIN);
  PD_DDR |= (1<<OUT_PIN)|(1<<OUT_INV_PIN);
  PD_ODR |= (1<<OUT_INV_PIN);
  PC_ODR &= ~(1<<TRIG_PIN);
}

void timer4_init(void)
{
  CLK_PCKENR1 = 0x10;
  TIM4_SR_bit.UIF = 0;
  TIM4_PSCR = 7;
  TIM4_ARR = 0xff;
  TIM4_CNTR = 0;
  TIM4_IER_bit.UIE = 1;
  //TIM4_CR1 |= MASK_TIM4_CR1_OPM;
  __enable_interrupt();
}

int main( void )
{
  sys_cfg();
  timer4_init();
  timer1_init();
  port_init();
  
  while(1)
    {
      PC_ODR |= (1<<TRIG_PIN);
      for(uint8_t i = 0; i!=38;i++);
      PC_ODR &= ~(1<<TRIG_PIN);
      TIM1_CR1 |= MASK_TIM1_CR1_CEN;  
      TIM4_CR1 |= MASK_TIM4_CR1_CEN;
      
      while(!stat);
      TIM1_CNTRH = 0;
      TIM1_CNTRL = 0;
      stat = 0;
    }
 
}

